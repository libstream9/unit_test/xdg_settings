#include <stream9/xdg/settings/scanner.hpp>

#include <system_error>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

namespace str = stream9::strings;
namespace settings = stream9::xdg::settings;

BOOST_AUTO_TEST_SUITE(scanner_)

    class event_recorder
    {
    public:
        struct event {
            int line_no;
            std::string message;

            bool operator==(event const&) const = default;
        };

        using next_action = settings::next_action;

    public:
        event_recorder(std::string_view const text)
            : m_text { text }
            , m_bol { m_text.begin() }
        {}

        void on_group_header(std::string_view const name, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "group header: " << name;

            m_events.emplace_back(m_line_no, message);
        }

        void on_entry(std::string_view const key,
                      std::string_view const value, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "entry: " << key << ", " << value;

            m_events.emplace_back(m_line_no, message);
        }

        void on_comment(std::string_view const text, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "comment: " << text;

            m_events.emplace_back(m_line_no, message);
        }

        void on_error(std::error_code const& e,
                      std::string_view const range, next_action&)
        {
            std::string message;
            str::ostream os { message };
            os << "error: " << e.message() << ", [" << range.begin() - m_bol
               << ", " << range.end() - m_bol << "]";

            m_events.emplace_back(m_line_no, message);
        }

        void on_new_line(std::string_view::iterator const it)
        {
            ++m_line_no;
            m_bol = it + 1;
        }

        auto const& events() const { return m_events; }

        friend std::ostream& operator<<(std::ostream& os, event const& e)
        {
            return os << e.line_no << ": " << e.message;
        }

    private:
        std::string_view m_text;
        int m_line_no = 1;
        std::string_view::iterator m_bol;
        std::vector<event> m_events;
    };

    BOOST_AUTO_TEST_CASE(basic_usage_)
    {
        auto const& text = "[Desktop Entry]\n"
            "Name=foo\n"
            "XXX\n"
            "\n"
            "[Desktop Action foo]\n"
            "Name[ja_JP.utf8]=xxx\n"
            "# comment\n"
            "[Desktop Action bar]\n"
            "Name=yyy\n"
            "[X-Foo Group A]\n"
            "Foo=bar\n"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "group header: Desktop Entry" },
            { 2, "entry: Name, foo" },
            { 3, "error: separator is expected, [3, 4]" },
            { 4, "comment: " },
            { 5, "group header: Desktop Action foo" },
            { 6, "entry: Name[ja_JP.utf8], xxx" },
            { 7, "comment: # comment" },
            { 8, "group header: Desktop Action bar" },
            { 9, "entry: Name, yyy" },
            { 10, "group header: X-Foo Group A" },
            { 11, "entry: Foo, bar" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_group_name_)
    {
        auto const& text = "[]\n" // empty!
            "name1 = value1\n" // should be skipped to a next group
            "[group 1]\n"
            "name2 = value2\n"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: empty group name, [1, 1]" },
            { 3, "group header: group 1" },
            { 4, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(group_name_contain_illegal_char_)
    {
        auto const& text = "[group[0]]\n" // group name can't contain '['
            "name1 = value1\n" // should be skipped to a next group
            "[group 1]\n"
            "name2 = value2\n"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: invalid group name, [6, 7]" },
            { 3, "group header: group 1" },
            { 4, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(group_name_contain_control_char_)
    {
        auto const& text = "[group\t0]\n" // group name can't control character
            "name1 = value1\n" // should be skipped to a next group
            "[group 1]\n"
            "name2 = value2\n"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: invalid group name, [6, 7]" },
            { 3, "group header: group 1" },
            { 4, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(group_header_doesnt_have_terminator_1_)
    {
        auto const& text = "[\n" // no closing ']'
            "name1 = value1\n" // should be skipped to a next group
            "[group 1]\n"
            "name2 = value2\n"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: terminator is expected, [1, 2]" },
            { 3, "group header: group 1" },
            { 4, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(group_header_doesnt_have_terminator_2_)
    {
        auto const& text = "[group 0\n" // no closing ']'
            "name1 = value1\n" // should be skipped to a next group
            "[group 1]\n"
            "name2 = value2\n"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: terminator is expected, [8, 9]" },
            { 3, "group header: group 1" },
            { 4, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(empty_key_)
    {
        auto const& text = "= value1\n" // empty!
            "name2 = value2"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: empty key, [0, 0]" },
            { 2, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(invalid_key_1_)
    {
        auto const& text = "\t\x01name = value1\n" // key can't contain control character
            "\tname2 = value2"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: invalid key, [1, 2]" },
            { 2, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(invalid_key_2_)
    {
        auto const& text = " name\x01 = value1\n" // key can't contain control character
            " name2 = value2"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: invalid key, [5, 6]" },
            { 2, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(no_separator_)
    {
        auto const& text = " name1 : value1\n" // colon isn't a separator
            " name2 = value2"
            ;

        event_recorder h { text };

        settings::scan(text, h);

        std::vector<event_recorder::event> const& expected = {
            { 1, "error: separator is expected, [7, 8]" },
            { 2, "entry: name2, value2" },
        };

        BOOST_TEST(h.events() == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // scanner_

} // namespace testing
